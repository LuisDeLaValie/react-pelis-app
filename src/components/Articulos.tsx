import React, { useState } from "react";
import { Peliculas } from "../data/Peliculas";
import { Editar } from "./Editar";

interface Props {
  pelicula: Peliculas;
  eliminar: (id: number) => void;
  editar: (id: Peliculas) => void;
}

export const Articulos = ({ pelicula, eliminar, editar }: Props) => {
  const [editando, setEditando] = useState(false);

  const terminarEdicion = (peli: Peliculas) => {
    setEditando(false);
    editar(peli);
  };

  return (
    <article className="peli-item">
      <h3 className="title">{pelicula.titulo}</h3>
      <p className="description">{pelicula.descripcion}</p>

      <button className="edit" onClick={() => setEditando(true)}>
        Editar
      </button>
      <button className="delete" onClick={() => eliminar(pelicula.id)}>
        Borrar
      </button>

      {editando ? (
        <Editar pelicula={pelicula} callback={terminarEdicion} />
      ) : null}
    </article>
  );
};
