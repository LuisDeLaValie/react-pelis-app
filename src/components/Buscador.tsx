import React, { ChangeEvent } from "react";

interface Props {
  buscar: (buscar: string) => void;
}

export const Buscador = ({ buscar }: Props) => {
  const chanegInput = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    buscar(e.target.value);
    
  };

  return (
    <div className="search">
      <h3 className="title">Buscador</h3>
      <form>
        <input type="text" id="search_field" onChange={chanegInput} />
        <button id="search">Buscar</button>
      </form>
    </div>
  );
};
