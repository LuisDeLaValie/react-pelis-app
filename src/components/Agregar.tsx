import React, { useState } from "react";
import { GuardarEnStorage } from "../helpers/GuardarEnStorage";
import { Peliculas } from "../data/Peliculas";

interface Props {
  agregarPelicula: (peli: Peliculas) => void;
}

export const Agregar = ({ agregarPelicula }: Props) => {
  const titulo = "Añadir Pelicula";
  const [peli, setPeli] = useState<Peliculas>();

  const generarLibro = (e: React.FormEvent<HTMLElement>) => {
    e.preventDefault();

    const targe = e.target;
    const titulo = targe.titulo.value;
    const descripcion = targe.descripcion.value;

    const newpeli = {
      id: new Date().getTime(),
      titulo: titulo,
      descripcion: descripcion,
      ceado: new Date(),
    };

    setPeli(newpeli);
    GuardarEnStorage("peliculas", newpeli);
    agregarPelicula(newpeli!);
  };

  return (
    <div className="add">
      <h3 className="title">{titulo}</h3>
      <strong>{peli?.titulo}</strong>
      <form onSubmit={generarLibro}>
        <input type="text" id="titulo" name="titulo" placeholder="Titulo" />

        <textarea
          id="descripcion"
          name="descripcion"
          placeholder="Descripción"
        ></textarea>
        <input type="submit" id="save" value="Guardar" />
      </form>
    </div>
  );
};
