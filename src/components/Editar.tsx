import React, { ChangeEvent, useState } from "react";
import { Peliculas } from "../data/Peliculas";

interface Props {
  pelicula: Peliculas;
  callback: (peli: Peliculas) => void;
}

export const Editar = ({ pelicula, callback }: Props) => {
  const [new_Pelicula, setNew_Pelicula] = useState(pelicula);

  const editarLibro = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const target = e.target;
    const titulo = target.titulo.value;
    const descripcion = target.descripcion.value;

    callback({ ...new_Pelicula, titulo: titulo, descripcion: descripcion });
  };

  const onChange = (e: ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    setNew_Pelicula({ ...new_Pelicula, [e.target.name]: e.target.value });
  };

  return (
    <div className="add">
      <h3>Editaraa</h3>
      <form onSubmit={editarLibro}>
        <input
          type="text"
          id="titulo"
          name="titulo"
          placeholder="Titulo"
          value={new_Pelicula.titulo}
          onChange={onChange}
        />

        <textarea
          id="descripcion"
          name="descripcion"
          placeholder="Descripción"
          value={new_Pelicula.descripcion}
          onChange={onChange}
        ></textarea>
        <input type="submit" id="save" value="Guardar" />
      </form>
    </div>
  );
};
