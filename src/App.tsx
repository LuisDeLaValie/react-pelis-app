import { useEffect, useState } from "react";
import { Agregar } from "./components/Agregar";
import { Articulos } from "./components/Articulos";
import { Buscador } from "./components/Buscador";
import { Layout } from "./layouts/Layout";
import { Peliculas } from "./data/Peliculas";

function App() {
  const [peliculas, setPeliculas] = useState<Peliculas[]>([]);

  useEffect(() => {
    obtenerPelicula();
  }, []);

  const obtenerPelicula = () => {
    const datos = JSON.parse(localStorage.getItem("peliculas") ?? "[]");

    setPeliculas(datos);
  };

  const agregarPelicula = (peli: Peliculas) => {
    setPeliculas([...(peliculas ?? []), peli]);
  };

  const eliminarPeli = (id: number) => {
    const new_list = peliculas?.filter((data) => data.id != id);

    localStorage.setItem("peliculas", JSON.stringify(new_list));
    setPeliculas(new_list);
  };

  const editarPelicula = (old: number, peli: Peliculas) => {
    const new_lista = peliculas;

    new_lista[old] = { ...peli };

    localStorage.setItem("peliculas", JSON.stringify(new_lista));
    setPeliculas([...new_lista]);
  };
  const buscarPelicula = (buscar: string) => {
    const datos = JSON.parse(
      localStorage.getItem("peliculas") ?? "[]"
    ) as Peliculas[];

    const busqueda = datos.filter(
      (item) =>
        item.titulo.toLocaleLowerCase().includes(buscar.toLocaleLowerCase()) ||
        item.descripcion
          .toLocaleLowerCase()
          .includes(buscar.toLocaleLowerCase())
    );
    setPeliculas(busqueda);
  };

  return (
    <Layout>
      {/* <!--Contenido principal--> */}
      <section id="content" className="content">
        {/* <!--aqui van las peliculas--> */}
        {peliculas?.length ?? 0 !== 0 ? (
          peliculas!.map((p, index) => (
            <Articulos
              key={p.id}
              pelicula={p}
              eliminar={eliminarPeli}
              editar={(e) => editarPelicula(index, e)}
            />
          ))
        ) : (
          <div> No se tinen peliculas</div>
        )}
      </section>

      {/* <!--Barra lateral--> */}
      <aside className="lateral">
        <Buscador buscar={buscarPelicula} />

        <Agregar agregarPelicula={agregarPelicula} />
      </aside>
    </Layout>
  );
}

export default App;
