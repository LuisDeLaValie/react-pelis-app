export function GuardarEnStorage(storage: string, data: unknown) {
  let listStorage = JSON.parse(localStorage.getItem(storage) ?? "[]");

  if (Array.isArray(listStorage)) {
    listStorage.push(data);
  } else {
    listStorage = [data];
  }
  console.log(listStorage);

  localStorage.setItem(storage, JSON.stringify(listStorage));
}
